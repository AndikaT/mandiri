package com.perpus.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.lang.Nullable;

@Entity
@Table(name = "biodata")
public class Biodata {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Long Id;
	
	@Nullable
	@Column(name="name", length=50)
	private String Name;
	
	@Nullable
	@Column(name="phone_num", length = 15)
	private String Phone_num;
	
	@Nullable
	@Column(name="domisili", length=50)
	private String Domisili;

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getPhone_num() {
		return Phone_num;
	}

	public void setPhone_num(String phone_num) {
		Phone_num = phone_num;
	}

	public String getDomisili() {
		return Domisili;
	}

	public void setDomisili(String domisili) {
		Domisili = domisili;
	}
}
