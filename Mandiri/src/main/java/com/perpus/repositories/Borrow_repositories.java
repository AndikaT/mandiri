package com.perpus.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.perpus.models.Borrow;

@Repository
public interface Borrow_repositories extends JpaRepository<Borrow, Long> {

}
