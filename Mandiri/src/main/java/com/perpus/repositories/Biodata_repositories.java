package com.perpus.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.perpus.models.Biodata;

@Repository
public interface Biodata_repositories extends JpaRepository<Biodata, Long> {

	@Query(value = "SELECT * FROM Biodata b WHERE b.id = MAX(b.id)", nativeQuery = true)
	List<Biodata> find_lastest();
}
